<?php

/**
 * @file
 * Database interface code for IBM DB2.
 */

/**
 * @ingroup database
 * @{
 */

include_once DRUPAL_ROOT . '/includes/database/prefetch.inc';

/**
* Regex for rewriting the DB2 reserved words , which are excluded in
* ANSI reserved word list.
*
* NOTE: Here we will assume that all table/column/constrain names are written
* in LOWER case, which also means all query element with UPPER case will not
* be checked. e.g. 'mode' will replace as '"mode"', but 'MODE' will keep
* untouched.
*
* NOTE: Check http://drupal.org/node/2497 for further information about
* Drupal's query coding standard.
*/
define('DB2_RESERVED_REGEXP', '/([[:space:],\.\(\)<>\+-=!*]|^)(activate|allow|associate|asutime|attributes|audit|aux|auxiliary|bufferpool|capture|cardinality|ccsid|cluster|collection|collid|concat|count_big|current_lc_ctype|current_schema|current_server|current_timezone|database|datapartitionname|datapartitionnum|days|db2general|db2genrl|db2sql|dbinfo|dbpartitionname|dbpartitionnum|defaults|definition|dense_rank|denserank|disable|disallow|dssize|editproc|enable|encoding|encryption|end-exec|ending|erase|every|excluding|exclusive|explain|fenced|fieldproc|file|final|generated|graphic|hash|hashed_value|hint|hours|including|inclusive|increment|index|inherit|integrity|isobid|jar|java|lc_ctype|linktype|localdate|locale|locators|lock|lockmax|locksize|long|maintained|materialized|maxvalue|microsecond|microseconds|minutes|minvalue|mode|months|new_table|nocache|nocycle|nodename|nodenumber|nomaxvalue|nominvalue|noorder|normalized|nulls|numparts|obid|old_table|optimization|optimize|overriding|package|padded|pagesize|part|partitioned|partitioning|partitions|password|piecesize|plan|prevval|priqty|program|psid|query|queryno|rank|recovery|refresh|rename|reset|restart|result_set_locator|row_number|rownumber|rowset|rrn|run|scratchpad|seconds|secqty|security|simple|sqlid|stacked|standard|starting|statement|stay|stogroup|stores|style|summary|synonym|sysfun|sysibm|sysproc|tablespace|validproc|variant|vcat|version|volatile|volumes|wlm|xmlelement|years)([[:space:],\.\(\)<>\+-=!*]|$)/Ds');
define('IBM_ROWNUM_ALIAS','RWN_TO_REMOVE'); // alias used for queryRange filtering (we have to remove that from resultsets)
define('IBM_MAX_VARCHAR_LENGTH',32766); // maximum length for a string value in a table column in ibm (affects schema.inc table creation)
define('IBM_MAX_COMMENT_LENGTH',200);

class DatabaseConnection_ibm extends DatabaseConnection {
	
	/**
	* Override of DatabaseConnection::driver().
	*/
	public function driver() {
		return 'ibm';
	}
	
	/**
	* Override of DatabaseConnection::databaseType().
	*/
	public function databaseType() {
		return 'DB2';
	}
	
	public function __construct(array $connection_options = array()) {
		
		// Store connection options for future reference.
		$this->connectionOptions = $connection_options;
		
		// We don't need a specific PDOStatement class here, we simulate it using
		// DatabaseStatement_ibm below.
		$this->statementClass = NULL;
		
		// This driver defaults to transaction support, except if explicitly passed FALSE.
		$this->transactionSupport = !isset($connection_options['transactions']) || $connection_options['transactions'] !== FALSE;
		
		// Construct connection string
		$dsn = 'ibm:DRIVER={IBM DB2 ODBC DRIVER};DATABASE='.$connection_options['database']. ';HOSTNAME='.$connection_options['host'].';PORT='.$connection_options['port'].';PROTOCOL=TCPIP;';
		
		parent::__construct($dsn, $connection_options['username'], $connection_options['password'], array(
		PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
		// Convert numeric values to strings when fetching.
		PDO::ATTR_STRINGIFY_FETCHES => TRUE,
		// Convert everything to lower
		PDO::ATTR_CASE => PDO::CASE_LOWER,
		));	
	}
	
	/**
	* Override of PDO::prepare(): prepare a prefetching database statement.
	*/
	public function prepare($query, $options = array()) {
		
		return new DatabaseStatement_ibm($this, $query, $options);
	}
	
	/**
	* Override of PDO::prepareQuery(): prepare a prefetching database statement.
	*/
	public function prepareQuery($query) {
		
		// @todo - look into caching the way ORA driver does
		$query= $this->escapeAnsi($query);
		$query= $this->escapeReserved($query);
		$query= $this->escapeCompatibility($query);
		$query= $this->prefixTables($query,true);
		$query= $this->escapeIfFunction($query);
		$query= $this->escapeDB2Specific($query);
		
		// Call our overriden prepare.
		return $this->prepare($query);
	}
	
	/**
	* Internal function: prepare a query by calling PDO directly.
	*
	* This function has to be public because it is called by other parts of the
	* database layer, but do not call it directly, as you risk locking down the
	* PHP process.
	*/
	public function PDOPrepare($query, array $options = array()) {
		return parent::prepare($query, $options);
	}
	
	// shortcut function to execute statements directly on the database without checks
	public function ibmQuery($query, $args=NULL)
	{
		$stmt= parent::prepare($query);
		try
		{
			$stmt->execute($args);
		}
		catch (Exception $e)
		{
			//syslog(LOG_ERR,"error: ".$e->getMessage()." ".$query . "in ibmQuery");
			$this->rollback();
			throw $e;
		}
	
		return $stmt;
	}
	
	public function query($query, array $args = array(), $options = array(), $retried = 0) {
		global $ibm_debug;
	
		// Use default values if not already set.
		$options += $this->defaultOptions();
	
		// We occassionally need to bypass the normal query functionality and do special things. We do that
		// here before a query is prepared/executed. Pass query to specialNeedsQuery() for handling.
		if(!($query instanceof PDOStatement) && preg_match('/GENERATED/i', $query)){
			$this->specialNeedsQuery($query,$args, $options);
		} else {
			try {
				if ($query instanceof PDOStatement) {
					//if ($ibm_debug) 
					//syslog(LOG_ERR,"Line 145 DatabaseConn::query: ".$query->queryString." args: ".print_r($args,true) . " options: " . print_r($options,true));
					$stmt = $query;
					$stmt->execute(NULL);
				}
				else {					
					$this->expandArguments($query, $args);
					$stmt = $this->prepareQuery($query);
					//if ($ibm_debug) 
					syslog(LOG_ERR,"Line 161 DatabaseConn::query: ". $stmt->queryString. " args: " . print_r($args,true) . " options: " . print_r($options,true));
					$stmt->execute($args, $options);	
				}
	
				switch ($options['return']) {
					case Database::RETURN_STATEMENT:
						//syslog(LOG_ERR,"This is Database::RETURN_STATEMENT");
						return $stmt;
					case Database::RETURN_AFFECTED:
						//syslog(LOG_ERR,"This is Database::RETURN_AFFECTED");
						return $stmt->rowCount();
					case Database::RETURN_INSERT_ID:
						//syslog(LOG_ERR,"This is Database::RETURN_INSERT_ID");
						return (isset($options['sequence_name']) ? $this->lastInsertId($options['sequence_name']) : false);
					case Database::RETURN_NULL:
						//syslog(LOG_ERR,"This is Database::RETURN_NULL");
						return;
					default:
						throw new PDOException('Invalid return directive: ' . $options['return']);
				}
			}
			catch (PDOException $e) {
				if ($options['throw_exception']) {
					
					if ($query instanceof PDOStatement)
					$query_string = $stmt->queryString;
					else
					$query_string = $query;
										
					// Add additional debug information.
					syslog(LOG_ERR,"error query: ". $query_string .(isset($stmt)&&$stmt instanceof DatabaseStatement_ibm ? " (prepared: ".$stmt->getQueryString()." )" : "")." e: ".$e->getMessage()." args: ".print_r($args,true) . " error code: " . $e->getCode());
					
					$ex= new PDOException($query_string .(isset($stmt)&&$stmt instanceof DatabaseStatement_ibm ? " (prepared: ".$stmt->getQueryString()." )" : "")." e: ".$e->getMessage()." args: ".print_r($args,true) . " error code: " . $e->getCode());
					
					if(!$e->getCode() == '24000') { // ignore fetch error on non-sql statements
						throw $e;
					}
				}
				return NULL;
			}
		}			
	}

	
	private function escapeAnsi($query)
	{
		 
		if (preg_match("/^select /i",$query)&&!preg_match("/^select(.*)from/ims",$query))
		$query.= ' FROM SYSIBM.SYSDUMMY1';
	
		$search = array ('/("\w+?")/e',
	                      "/([^\s\(]+) & ([^\s]+) = ([^\s\)]+)/",
	                      "/([^\s\(]+) & ([^\s]+) <> ([^\s\)]+)/" // bitand
		/*'/^RELEASE SAVEPOINT (.*)$/'*/);
	
		$replace = array ("strtoupper('\\1')",
	                       "BITAND(\\1,\\2) = \\3",
	                       "BITAND(\\1,\\2) <> \\3"
		/*"begin null; end;"*/);
	
		return str_replace('\\"','"',preg_replace($search, $replace, $query));
			
	}
	
	private function escapeIfFunction($query) {
		return preg_replace("/IF\s*\((.*?),(.*?),(.*?)\)/", 'case when \1 then \2 else \3 end', $query);
	}
	
	private function escapeDB2Specific($query)
	{
		$query = str_replace("\\", "'", $query);
		return $query;
	}
	
	private function escapeReserved($query)  {
	
		return $query = preg_replace(DB2_RESERVED_REGEXP, '\\1"\2"\\3', $query);
	}
	
	private function escapeCompatibility($query) {
	
		$search = array ("''||", // remove empty concatenations leaved by concatenate_bind_variables
		                     "||''",
		                     "IN ()", // translate 'IN ()' to '= NULL' they do not match anything anyway (always false)
		                     "IN  ()",
		                     '(FALSE)',
		                     'POW(',
		                     ") AS count_alias", // ugly hacks here
			                 '"{URL_ALIAS}" GROUP BY path',
			                  "ESCAPE '\\\\'",
	                       'SUBSTRING_INDEX(',
	                       'SUBSTRING(',
	                       "parameters = ''",
						   'COUNT(DISTINCT(message))'
		);
	
		$replace = array ("",
		                      "",
		                      "= NULL",
		                      "= NULL",
		                      "(1=0)",
		                      "POWER(",
		                      ") count_alias",// ugly hacks replace strings here
			                  '"{URL_ALIAS}" GROUP BY SUBSTRING_INDEX(source, \'/\', 1)',
			                  "ESCAPE '!'",
	                      'SUBSTRING.SUBSTRING_INDEX(',
	                      'SUBSTRING.SUBSTRING(',
	                      "parameters IS NULL", //ugly hack to deal with operand on blob
	                      // dblog doesn't build querys, its add COUNT/DISTINCT as an expression. We could parse
						  // and add '..md5sum' but that is ugly also. Replacing for now as this probably
						  // won't occur in any other situations.
						  'COUNT(DISTINCT(message..md5sum))'
		);
	
		return str_replace($search, $replace, $query);
	}
	
	/**
	* Override of DatabaseConnection::pushTransaction().
	*/
	public function pushTransaction($name) {
		if (!$this->supportsTransactions()) {
			return;
		}
		if (isset($this->transactionLayers[$name])) {
			throw new DatabaseTransactionNameNonUniqueException($name . " is already in use.");
		}
		// If we're already in a transaction then we want to create a savepoint
		// rather than try to create another transaction.
		if ($this->inTransaction()) {
			$this->query('SAVEPOINT ' . $name . ' ON ROLLBACK RETAIN CURSORS');
		}
		else {
			parent::beginTransaction();
		}
		$this->transactionLayers[$name] = $name;
	}
	
	/**
	* Override of DatabaseConnection::rollback().
	*/
	public function rollback($savepoint_name = 'drupal_transaction') {
		if (!$this->supportsTransactions()) {
			return;
		}
		if (!$this->inTransaction()) {
			throw new DatabaseTransactionNoActiveException();
		}
		// A previous rollback to an earlier savepoint may mean that the savepoint
		// in question has already been rolled back.
		if (!in_array($savepoint_name, $this->transactionLayers)) {
			return;
		}
	
		// We need to find the point we're rolling back to, all other savepoints
		// before are no longer needed. If we rolled back other active savepoints,
		// we need to throw an exception.
		$rolled_back_other_active_savepoints = FALSE;
		while ($savepoint = array_pop($this->transactionLayers)) {
			if ($savepoint == $savepoint_name) {
				// If it is the last the transaction in the stack, then it is not a
				// savepoint, it is the transaction itself so we will need to roll back
				// the transaction rather than a savepoint.
				if (empty($this->transactionLayers)) {
					break;
				}
				$this->query('ROLLBACK TO ' . $savepoint);
				$this->popCommittableTransactions();
				if ($rolled_back_other_active_savepoints) {
					throw new DatabaseTransactionOutOfOrderException();
				}
				return;
			}
			else {
				$rolled_back_other_active_savepoints = TRUE;
			}
		}
		parent::rollBack();
		if ($rolled_back_other_active_savepoints) {
			throw new DatabaseTransactionOutOfOrderException();
		}
	}
	
	
	// We have a table called IDENTITY_COLUMN which keep a record of all tables with IDENTITY columns and their
	// primary key values. Much more optimal than querying and looping to find sequences. In schema.inc we insert
	// the value of (TABLE_NAME,COLUMN_NAME) However, a trigger is unable to set the SEQUENCE_NAME sice the table
	// isn't created till after trigger execution causing a deadlock.  Here, we parce the query,
	public function setSequenceName($query){
	
	$table = explode(" ", $query); // Get table_name
	// Then update the SEQUENCE_NAME with 'NULL' -> which executeS the trigger to set SEQUENCE_NAME
	$this->exec("UPDATE IDENTITY_COLUMN SET SEQUENCE_NAME = 'NULL' WHERE TABLE_NAME = '". $table[2] ."'");
	
	}
	
	/**
	* Retrive a the next id in a sequences. MySQL install uses a sequence table to
	* insert/update values. DB2 has built in sequences. We'll use these instead of
	* inserting and updating a sequences table. Our sequence object in (resources/sequenes)
	* is created during install. The table is still there, we just don't use it.
	* IMPORTANT: This sequence object differ from the system sequence objects created
	* when GENERATING IDENTITY columns. These objects have much more flexibility in case
	* we need to do things later:
	* http://db2portal.blogspot.com/2006/09/sequence-objects-and-identity-columns.html
	* This is almost exactly the way the Oracle driver does it.
	*/
	public function nextId($existing = 0) {
	
		$sequence_name = strtoupper($this->makeSequenceName('sequences', 'value'));
		syslog(LOG_ERR,"USING sequence for nextId() seq_name: " . $sequence_name);
		$id = $this->query("values (nextval for " . $sequence_name .")" )->fetchField();
	
		if ($id > $existing) {
			return $id;
		}
		 
		// advance sequence - this is common for all drivers that use sequences
		$this->query("ALTER SEQUENCE ". $sequence_name . " RESTART WITH " . ($id +1));
	
		return $id;
	}
	
	public function mapConditionOperator($operator) {
	
		return NULL; // THIS IS WHAT ORA DOES
	}
	
	public function queryRange($query, $from, $count, array $args = array(), array $options = array()) {
		 	 
		$qr = $query . ' LIMIT ' . (int) $count . ' OFFSET ' . (int) $from;
		syslog(LOG_ERR,"queryRange called for: " . $qr . " args: " . print_r($args,true) . " options: " . print_r($options,true));
		return $this->query($qr, $args, $options);
		//return $this->query($query . ' LIMIT ' . (int) $count . ' OFFSET ' . (int) $from, $args, $options);
	}
	
	public function queryTemporary($query, array $args = array(), array $options = array()) {
		syslog(LOG_ERR,"queryTemporary called for: " . $query);
		$tablename = $this->generateTemporaryTableName();
		$this->query(preg_replace('/^SELECT/i', 'CREATE TEMPORARY TABLE {' . $tablename . '} AS SELECT', $query), $args, $options);
		return $tablename;
	}
		
	/* Rather than runnning things through a massive/ugly & if/else sequence in query(), we're going to pass
	* everything that matches a condition and pass it to the this 'special needs' handler */
	public function specialNeedsQuery($query,$args = array(), $options = array()){
		
		syslog(LOG_ERR,"specialNeedsQuery called for " . $query);
		/* Clean up this regex later */
		$query = preg_replace('/\{/', '', $query);
		$query = preg_replace('/\}/', '', $query);
		$query = preg_replace("/\"/", '', $query);
	
		/* All queries don't need regex but it won't hurt anything if we do it*/
		if(preg_match('/GENERATED/i', $query)) {
			try {
				$stmt = $this->prepareQuery($query);
				//if ($ibm_debug) syslog(LOG_ERR,"query: ".$stmt->getQueryString() ." args: ".print_r($args,true));
				syslog(LOG_ERR,"GENERATED BEING HANDLED IN SPECIAL NEEDS!! query: " . $query);
				$stmt->execute($args, $options);
				$this->setSequenceName($query);
			} catch (Exception $e) {
				syslog(LOG_ERR,"Error WITH GENERATED!! Check specialNeedsQuery " . $e->getMessage());
			}
		}
		
		if(preg_match('/COUNT\(DISTINCT\(/i', $query)) {
			$parts = explode(')',$query);
			$query = $parts[0]. "..md5sum))" . $parts[2] . ")";
			syslog(LOG_ERR,"Exception Caught: 4285 NEW QUERY => " . $query  . " args: " . print_r($args,true) . " options: " . print_r($options,true));
			$stmt = $this->prepareQuery($query);
			//$stmt->getColumnCount($query);
			try {
			//	$this->query($query,$args,$options);
			$stmt->execute($args, $options);
			} catch (Exception $e) {
				syslog(LOG_ERR,"Error WITH COUNT-DISTINCT!! Check specialNeedsQuery " . $e->getMessage());
			}
		}
		
		
	}
	
	// This function is from Oracle's driver, we can use something similar to get the last_insert_id of
	// IDENTITY columns in DB2
	public function lastInsertId($name=NULL) {
		syslog(LOG_ERR,"lastInsertId for sequence => " . $name);
		if (!$name)
		throw new Exception('The name of the sequence is mandatory for Oracle');
	
		try
		{
			return $this->ibmQuery($this->prefixTables("SELECT LASTASSIGNEDVAL FROM SYSIBM.SYSSEQUENCES WHERE SEQNAME='".$name."'",true))->fetchColumn();
		}
		catch (Exception $ex)
		{
			/* ignore if CURRVAL not set (may be an insert that specified the serial field) */  }
	}
	
	/**
	* Override of DatabaseConnection::version().
	*/
	public function version() {
		// PDO::ATTR_SERVER_VERSION doesn't play with PDO_IBM
		return $this->query("SELECT SERVICE_LEVEL FROM SYSIBMADM.ENV_INST_INFO")->fetchField();
	}
	
} // END DatabaseConnection_ibm

class DatabaseStatement_ibm extends DatabaseStatementPrefetch implements Iterator, DatabaseStatementInterface {
	
	public $queryString;
	
	public function __construct(DatabaseConnection $connection, $query, array $driver_options = array()) {
		$this->dbh = $connection;
		$this->queryString = $query;
		$this->driverOptions = $driver_options;
	}
	
	public function getStatement($query, &$args = array()) {
		syslog(LOG_ERR,"getStatement query: ");
		return $this->dbh->PDOPrepare($this->queryString);
	}
	
	public function getColumnCount($query) {
		
	}
	
	public function execute($args = array(), $options = array()) {
		if (isset($options['fetch'])) {
			if (is_string($options['fetch'])) {
				// Default to an object. Note: db fields will be added to the object
				// before the constructor is run. If you need to assign fields after
				// the constructor is run, see http://drupal.org/node/315092.
				$this->setFetchMode(PDO::FETCH_CLASS, $options['fetch']);
			}
			else {
				$this->setFetchMode($options['fetch']);
			}
		}
	
		$logger = $this->dbh->getLogger();
		if (!empty($logger)) {
			$query_start = microtime(TRUE);
		}
	
		// Prepare the query.
		$statement = $this->getStatement($this->queryString, $args);
		if (!$statement) {
			$this->throwPDOException();
		}
	
		$return = $statement->execute($args);
		if (!$return) {
			$this->throwPDOException();
		}
	
		// Fetch all the data from the reply, in order to release any lock
		// as soon as possible.
		$this->rowCount = $statement->rowCount();
		
		// don't need this atm
/* 		$null = array();
		if(isset($options['column_count'])){	
			for ($i = 0; $i < $options['column_count']; $i++) {
				$meta = $statement->getColumnMeta($i);
				if ($meta['native_type'] == 'BLOB') {
					syslog(LOG_ERR,"Line 504 DatabaseStmt::query bindColumn for query: " . $this->queryString);
					$null[$i] = NULL;
					$statement->bindColumn($i + 1, $null[$i], PDO::PARAM_LOB);
				}
			}
		} */
		
		try {
		$this->data = $statement->fetchAll(PDO::FETCH_ASSOC);
		} catch (Exception $e) {
			if(!$e->getCode() == '24000') // ignore non-fetchable statements errors
			throw $e;
		}
		// Destroy the statement as soon as possible. See
		// DatabaseConnection_sqlite::PDOPrepare() for explanation.
		unset($statement);
	
		$this->resultRowCount = count($this->data);
	
		if ($this->resultRowCount) {
			$this->columnNames = array_keys($this->data[0]);
			//syslog(LOG_ERR, "Line 517 DatabaseStatement_ibm columnNames => " . print_r($this->columnNames, true));
		}
		else {
			$this->columnNames = array();
			//syslog(LOG_ERR, "Line 521 DatabaseStatement_ibm columnNames => " . print_r($this->columnNames, true));
		}
	
		if (!empty($logger)) {
			$query_end = microtime(TRUE);
			$logger->log($this, $args, $query_end - $query_start);
		}
	
		// Initialize the first row in $this->currentRow.
		$this->next();
	
		return $return;
	}
	
}
