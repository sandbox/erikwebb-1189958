<?php

/**
 * @file
 * Select builder for PostgreSQL database engine.
 */

/**
 * @ingroup database
 * @{
 */

class SelectQuery_ibm extends SelectQuery {
	
	
	
	public function __toString() {
		
		// For convenience, we compile the query ourselves if the caller forgot
		// to do it. This allows constructs like "(string) $query" to work. When
		// the query will be executed, it will be recompiled using the proper
		// placeholder generator anyway.
		if (!$this->compiled()) {
			$this->compile($this->connection, $this);
		}
	
		// Create a sanitized comment string to prepend to the query.
		$comments = $this->connection->makeComment($this->comments);
	
		// SELECT
		$query = $comments . 'SELECT ';
		if ($this->distinct) {
			$query .= 'DISTINCT ';
		}
	
		// FIELDS and EXPRESSIONS
		$fields = array();
		foreach ($this->tables as $alias => $table) {
			if (!empty($table['all_fields'])) {
				$fields[] = $this->connection->escapeTable($alias) . '.*';
			}
		}
		foreach ($this->fields as $alias => $field) {
			// Always use the AS keyword for field aliases, as some
			// databases require it (e.g., PostgreSQL).
			$fields[] = (isset($field['table']) ? $this->connection->escapeTable($field['table']) . '.' : '') . $this->connection->escapeField($field['field']) . ' AS ' . $this->connection->escapeAlias($field['alias']);		
		}
		foreach ($this->expressions as $alias => $expression) {
			$fields[] = $expression['expression'] . ' AS ' . $this->connection->escapeAlias($expression['alias']);
		}
		$query .= implode(', ', $fields);
	
	
		// FROM - We presume all queries have a FROM, as any query that doesn't won't need the query builder anyway.
		$query .= "\nFROM ";
		foreach ($this->tables as $alias => $table) {
			$query .= "\n";
			if (isset($table['join type'])) {
				$query .= $table['join type'] . ' JOIN ';
			}
	
			// If the table is a subquery, compile it and integrate it into this query.
			if ($table['table'] instanceof SelectQueryInterface) {
				// Run preparation steps on this sub-query before converting to string.
				$subquery = $table['table'];
				$subquery->preExecute();
				$table_string = '(' . (string) $subquery . ')';
			}
			else {
				$table_string = '{' . $this->connection->escapeTable($table['table']) . '}';
			}
	
			// Don't use the AS keyword for table aliases, as some
			// databases don't support it (e.g., Oracle).
			$fromString = $table_string . ' ' . $this->connection->escapeTable($table['alias']);
			$fromStringArray[] = $fromString;
			$query .=  $fromString;
	
			if (!empty($table['condition'])) {
			$query .= ' ON ' . $table['condition'];
	    }
			}
			
			// WHERE
			if (count($this->where)) {
			// There is an implicit string cast on $this->condition.
				$query .= "\nWHERE " . $this->where;
			}
	
			// GROUP BY
			if ($this->group) {
				$query .= "\nGROUP BY " . implode(', ', $this->group);
				// DB2 doesn't allow for GROUP BY on blobs. Some drupal queries do GROUP BY
				// on blobs and others on big:text which we the UDT 'bigtext_t'. We are going
				// to  the query build above and then only modify if we have reason to.
				$blob_group = $this->checkGroupByFields($this->tables);
				if(!is_null($blob_group)){
					// Modificaton begins by splitting the query into a SELECT part and a GROUP BY part
					$parts = preg_split('/GROUP BY/',$query);
					$select_str = $parts[0];
					$group_str = "GROUP BY" . $parts[1];
					
					// We need and array to hold the select values of the outer query since we are going to nest the
					// existing query
					$outer_fields = array();
					// Begin processing those columns (blob,bigtext_t) that need special attention
					foreach ($blob_group as $row) {
						foreach ($row as $key => $val){ // $key => TABLE_NAME, $val => TYPE (blob | bigtext_t)
							// Need to split the key (i.e. - w.field as field) into aliased and non-aliased parts
							$field = explode(' ',$key);
							$aliased =  $field[0];
							$non_aliased =  $field[2];
							// We also need to make sure these fields aren't added to to the outer_fields since we manually add those
							// To do this we need to unset values in the $fields array which we will use at the end
							// to add aliases to the outer SELECT part
							while(in_array($key, $fields)){
								$position = array_search($key, $fields);
								unset($fields[$position]);
							}

							switch ($val){
								case 'BLOB':
									$select_str = preg_replace("/$aliased/", "CAST($aliased AS VARCHAR(200))", $select_str);
									$group_str = preg_replace("/$non_aliased/", "CAST($non_aliased AS VARCHAR(200))", $group_str);
									break;
								case 'BIG_T':
									$select_str = preg_replace("/$aliased/", "CAST($aliased..content AS VARCHAR(200))", $select_str);
									$group_str = preg_replace("/$non_aliased/", "CAST($non_aliased..content AS VARCHAR(200))", $group_str);
									break;
							}
							// We need to re-cast these fields back to BLOB for PDO
							$outer_fields[] = "BLOB($non_aliased) AS $non_aliased";
						}
					}
					// All columns selected in the inner query will have an alias. We only need to SELECT those
					// aliases in the outer query. We unset the current value and set only the 'AS {expression}'
					foreach($fields as $key => $field) {
						//syslog(LOG_ERR,"this->fields key: " . $key . " val: " . $field);
						$col = explode(' ', $field);
						unset($fields[$key]);
						$fields[$key] = $col[2];
					}
					
					// Once all processing is done, lets rebuild the new query
					$outer_fields = array_merge($outer_fields,$fields);
					$outer_str = "SELECT " . implode(', ', $outer_fields) . " FROM (";
					$query = $outer_str. $select_str . $group_str . ")";
					//syslog(LOG_ERR,"TMP_QUERY: " . $tmp_query);
				//syslog(LOG_ERR,"SelectQuery fields[] => " . print_r($fields,true) . " from[] " . print_r($this->tables,true) . " this->group[] => " . print_r($this->group,true));
				}
			}
	
			// HAVING
			if (count($this->having)) {
			// There is an implicit string cast on $this->having.
				$query .= "\nHAVING " . $this->having;
			}
	
			// ORDER BY
			if ($this->order) {
			$query .= "\nORDER BY ";
	    $fields = array();
			foreach ($this->order as $field => $direction) {
			$fields[] = $field . ' ' . $direction;
			}
	    $query .= implode(', ', $fields);
			}
	
			// RANGE
			// There is no universal SQL standard for handling range or limit clauses.
			// Fortunately, all core-supported databases use the same range syntax.
			// Databases that need a different syntax can override this method and
			// do whatever alternate logic they need to.
			if (!empty($this->range)) {
			$query .= "\nLIMIT " . (int) $this->range['length'] . " OFFSET " . (int) $this->range['start'];
			}
	
			// UNION is a little odd, as the select queries to combine are passed into
			// this query, but syntactically they all end up on the same level.
			if ($this->union) {
			foreach ($this->union as $union) {
			$query .= ' ' . $union['type'] . ' ' . (string) $union['query'];
	    }
			}
	
			if ($this->forUpdate) {
			$query .= ' FOR UPDATE';
	}
	
			return $query;
  }
	
  public function execute() {
		// If validation fails, simply return NULL.
		// Note that validation routines in preExecute() may throw exceptions instead.
		if (!$this->preExecute()) {
			return NULL;
		}
		$args = $this->getArguments();
		
		// This isn't doing anything right now. In DatabaseStatement we may need to bind cols (using PARAM_LOB)
		// Inorder to do this we need the col_count and we can't get it from getColumnCount since we don't have
		// a PDOStatment
		if(count($this->fields) > 1) // only 1 field it couldn't be a blob
		$this->queryOptions['column_count'] = count($this->fields);

		return $this->connection->query((string) $this, $args, $this->queryOptions);
  }
  
  

  public function orderRandom() {
    $alias = $this->addExpression('RANDOM()', 'random_field');
    $this->orderBy($alias);
    return $this;
  }
  
  private function checkDistintBlob() {
  	
  }
  
  private function checkGroupByFields ($tables) {
  	// We don't know if there will be a GROUP BY situation with multiple tables, so we need to handle those situations
  	// We need to build a query string where TABLE_NAME = '' OR TABLE_NAME = ......
  	$whereString = 'TABLE_NAME = ';
  	$i = 0;
  	foreach($tables as $table) {
  		if($i == 0) {
  		$whereString .= "'". $table['table'] ."'";
  		
  		$i += 1;
  		} else {
  			$whereString .= ' OR TABLE_NAME = ' . "'". $table['table'] ."'";
  		}
  	}
  	
  	$query = "select CONCAT(CONCAT('w.',FIELD_NAME), CONCAT(' AS ', FIELD_NAME)) AS FIELD_NAME, CASE WHEN 1 = 1 THEN 'BIG_T' END AS TYPE from BIGTEXT_COLUMN where $whereString UNION select CONCAT(CONCAT('w.',FIELD_NAME), CONCAT(' AS ', FIELD_NAME)) AS FIELD_NAME, CASE WHEN 1 = 1 THEN 'BLOB' END AS TYPE from BLOB_COLUMN where $whereString";
  	$special_cols[] = db_query($query)->fetchAllKeyed();
  	foreach ($special_cols as $row) {
  		if(count($row) === 0) {
  			return null; // if there are no fields we return null
  		} else {
  			return $special_cols; // if there are values we pass back a keyed array
  		}
  	}
  }
  
  
  
/*   public function __toString(){
    $query = parent::__toString();
    // CONSIDER PUTTING INTO METHOD
    if($this->order && $this->expressions)  {
    $parts = preg_split('/ORDER BY/i',$query);
    $query = 'SELECT * FROM('.$parts[0].') ORDER BY '.$parts[1];
    }
    return $query;
  } */
    
  /* @todo: fix this- "FOR UPDATE" is a good feature, look declaring a new cursor
   * 
   * */
/*   public function forUpdate($set = TRUE) {
    // DB2 does support for update - but can't tell if it's working properly
    return $this;
  } */

}

/**
 * @} End of "ingroup database".
 */

