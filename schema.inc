<?php

/**
 * @file
 * Database schema code for PostgreSQL database servers.
 */

/**
 * @ingroup schemaapi
 * @{
 */

class DatabaseSchema_ibm extends DatabaseSchema {

  /**
   * A cache of information about blob columns and sequences of tables.
   *
   * This is collected by DatabaseConnection_ibm->queryTableInformation(),
   * by introspecting the database.
   *
   * @see DatabaseConnection_ibm->queryTableInformation()
   * @var array
   */
  protected $tableInformation = array();

  /**
   * Fetch the list of blobs and sequences used on a table.
   *
   * We introspect the database to collect the information required by insert
   * and update queries.
   *
   * @param $table_name
   *   The non-prefixed name of the table.
   * @return
   *   An object with two member variables:
   *     - 'blob_fields' that lists all the blob fields in the table.
   *     - 'sequences' that lists the sequences used in that table.
   */
  public function queryTableInformation($table) {
    
    syslog(LOG_ERR,"SCHEMA::FUNC: queryTableInformation");
    // Generate a key to reference this table's information on.
    // @todo: Deal w/ prefixing later
    $key = 'demo.'.$table;
    //$key = $this->connection->prefixTables('{' . $table . '}');
    
    //$table_information = $this->getPrefixInfo($table);
    //$key = $table_information['schema'] . '.' . $table_information['table'];
    

    if (!isset($this->tableInformation[$key])) {
      // Split the key into schema and table for querying.
      list($schema, $table_name) = explode('.', $key);
      $table_information = (object) array(
        'blob_fields' => array(),
        'bigtext_fields' => array(),
        'sequences' => array()
      );
      
      // @todo - this produces some big loops, fix
      syslog(LOG_ERR,'SCHEMA::FUNC: $schema => ' . $schema . ' $table_name => ' . $table_name . '  $key => ' .$key);
      
      // Get BLOBS - instead of looping through all columns or using a complex query to retrieve all blob columns
      // for a table, we keep a list of all blob columns in a table and then search for the table in our query
      $blobs = $this->connection->query("SELECT FIELD_NAME column_name FROM BLOB_COLUMN WHERE TABLE_NAME = :table", array(
      ':table' => $table_name
      ));

      // Load BLOBS
      foreach ($blobs as $column) {
      	// strtolower breaks things here
          $table_information->blob_fields[$column->column_name] = TRUE;
          syslog(LOG_ERR,'SCHEMA::FUNC: WE HAVE A BLOB => ' . $column->column_name);
      }
      
      $big_texts = $this->connection->query("SELECT FIELD_NAME column_name FROM BIGTEXT_COLUMN WHERE TABLE_NAME = :table", array(
            ':table' => $table_name
      ));
      
      // Load BIGTEXT columns
      foreach ($big_texts as $column) {
      	// strtolower breaks things here
      	$table_information->bigtext_fields[$column->column_name] = TRUE;
      	syslog(LOG_ERR,'SCHEMA::FUNC: WE HAVE A BIGTEXT => ' . $column->column_name);
      }
      
        
      // Get SEQUENCES
      //$sequence = $this->connection->query("SELECT column_name column_name, sequence_name sequence_name FROM IDENTITY_COLUMN WHERE TABLE_NAME = '".$table_name."'");
      $sequence = $this->connection->query("SELECT column_name column_name, sequence_name sequence_name FROM IDENTITY_COLUMN WHERE TABLE_NAME = :table", array(
      ':table' => $table_name
      ));
      
      // Load SEQUENCES - these don't need to be arrays since there will only ever be one per table
      foreach ($sequence as $column) {
          $table_information->sequence = $column->sequence_name;
          $table_information->serial_field = $column->column_name;
      }  
      
      $this->tableInformation[$key] = $table_information;
    }
    return $this->tableInformation[$key];
  }

  /**
   * Generate SQL to create a new table from a Drupal schema definition.
   *
   * @param $name
   *   The name of the table to create.
   * @param $table
   *   A Schema API table definition array.
   * @return
   *   An array of SQL statements to create the table.
   */
  protected function createTableSql($name, $table) {
    
  	syslog(LOG_ERR,"FUNCTION: createTableSql - table_name is: " . $name);

  	
    $sql_fields = array();
    $blob_col_insert = null;
    foreach ($table['fields'] as $field_name => $field) {
      $sql_fields[] = $this->createFieldSql($field_name, $this->processField($name,$field,$field_name));
      
      // if 'type' is serial we update the IDENTITY_COLUMN table. It's ok to do this in a foreach loop
      // since there will only ever be at most 1 field declared as serial
      if ($field['type'] == 'serial'):
        $this->updateIdentiesTable($table['name'],$field_name);
      endif;
      
    }
    
    $sql_keys = array();
    if (isset($table['primary key']) && is_array($table['primary key'])) {
      $sql_keys[]= 'CONSTRAINT '. 'PK_'.$name.' PRIMARY KEY ('.$this->createColsSql($table['primary key']).')';
    }
    
    if (isset($table['unique keys']) && is_array($table['unique keys'])) {
      foreach ($table['unique keys'] as $key_name => $key) {
        $sql_keys[]= 'CONSTRAINT '. 'UK_'.$name.'_'.$key_name .' UNIQUE ('.$this->createColsSql($key).')';
      }
    }

    $sql = "CREATE TABLE {" . $name . "} (\n\t";
    $sql .= implode(",\n\t", $sql_fields);
    if (count($sql_keys) > 0) {
      $sql .= ",\n\t";
    }
    $sql .= implode(",\n\t", $sql_keys);
    $sql .= "\n)";
    $statements[] = $sql;

    if (isset($table['indexes']) && is_array($table['indexes'])) {
      foreach ($table['indexes'] as $key_name => $key) {
        $statements[] = $this->_createIndexSql($name, $key_name, $key);
      }
    }

    // Add table comment.
    if (!empty($table['description'])) {
      $statements[] = 'COMMENT ON TABLE ' . strtoupper($name) . ' IS ' . $this->prepareComment($table['description'],$name, null);
    }

    // Add column comments.
    foreach ($table['fields'] as $field_name => $field) {
      if (!empty($field['description'])) {
        $statements[] = 'COMMENT ON COLUMN ' . strtoupper($name) . '.' . $field_name . ' IS ' . $this->prepareComment($field['description'], $name, $field);
      }
    }
        
    return $statements;
  }
  
  // To avoid unecessary looping through table columns in order to locate identity/blob columns, and
  // to locate the sequence object, we store all relevant into in the BLOB_COLUMN, IDENTITY_COLUMN tables
  // that we created when install was initialized
  public function updateIdentiesTable ($table, $field_name){

    $this->connection->query("INSERT INTO IDENTITY_COLUMN (table_name,column_name) VALUES (:table,:field_name)",array(
		':table' => $table,
		':field_name' => $field_name
    ));
  }
  
  /**
   * Create an SQL string for a field to be used in table creation or
   * alteration.
   *
   * Before passing a field out of a schema definition into this
   * function it has to be processed by _db_process_field().
   *
   * @param $name
   *    Name of the field.
   * @param $spec
   *    The field specification, as per the schema data structure format.
   */
  protected function createFieldSql($name, $spec) {
    
    syslog(LOG_ERR,"FUNCTION: createFieldSql 'name' is: " . $name. " spec[type] is " . $spec['type']);
    $sql = $name . ' ';
    
    // According to MySQL, unsiged is equal to SERIAL is an alias for: 
    // BIGINT UNSIGNED NOT NULL AUTO_INCREMENT UNIQUE.
    // We will try to replicate this, don't need to set anything else so we bypass if 
    // serail condition is met.
    if (isset($spec['type']) && $spec['type'] == 'serial') {
      $sql .= ' BIGINT GENERATED BY DEFAULT AS IDENTITY (START WITH 1, INCREMENT BY 1, NOCACHE)'; /*(START WITH 1, INCREMENT BY 1)*/ 
      unset($spec['not null']);
    } else {
    $sql .=  $spec['ibm_type'] . ' ';
    if (in_array($spec['ibm_type'], array('varchar', 'character', 'text')) && isset($spec['length'])) {
      $sql .= '(' . $spec['length'] . ')';
    }
    elseif (isset($spec['precision']) && isset($spec['scale'])) {
      $sql .= '(' . $spec['precision'] . ', ' . $spec['scale'] . ')';
    }
    
    // if using custom type bigtext_t we need to remove null and default values. these have been move to the "CREATE TYPE" statement at install startup
    if($spec['ibm_type'] == 'bigtext_t'){
    	unset($spec['not null']);
    	unset($spec['default']);
    } 
    
    if (isset($spec['not null'])) {
      if ($spec['not null']) {
        $sql .= ' NOT NULL ';
      }
      else {
        $sql .= ' NULL ';
      }
    }
    
    // $spec['default'] can be NULL, so we explicitly check for the key here.
    if (array_key_exists('default', $spec)) {
      if (is_string($spec['default'])) {
        $spec['default'] = "'" . $spec['default'] . "'";
      }
      elseif (!isset($spec['default'])) {
        $spec['default'] = 'NULL';
      }
      $sql .= ' DEFAULT ' . $spec['default'];
    }
	
    if (empty($spec['not null']) && !isset($spec['default']) && $spec['ibm_type'] != 'bigtext_t') {
      $sql .= ' DEFAULT NULL';
    }
    
    } // end of outer if...else
       
    return $sql;
  }

  /**
   * Set database-engine specific properties for a field.
   *
   * @param $field
   *   A field description array, as specified in the schema documentation.
   */
  protected function processField($table,$field,$field_name) {
      	
    // We really don't need to receive $table & field_name variables here, all other drivers don't
    // It's needed so we can add records to BLOB_COLUMN table and we can only do so
    // after processField has mapped fields(converted text:big -> blob)
    
    if (!isset($field['size'])) {
      $field['size'] = 'normal';
    }

    /* Ora driver handles this differently, sticking with default for now, if issues see Ora*/
    if (!isset($field['ibm_type'])) {
      $map = $this->getFieldTypeMap();
      $field['ibm_type'] = $map[$field['type'] . ':' . $field['size']];
    }
    
    // Add records to BLOB_COLUMN if 'ibm_type' is a blob
    if ($field['ibm_type'] == 'blob'):
        $this->connection->query("INSERT INTO BLOB_COLUMN VALUES (:table,:column)",array(
        ':table' => $table,
        ':column' => $field_name // This needs to be LOWER!!
        ));
    endif;
    
    // Add records to BIGTEXT_COLUMN if 'ibm_type' is a bigtext_t
    if ($field['ibm_type'] == 'bigtext_t'):
    $this->connection->query("INSERT INTO BIGTEXT_COLUMN (table_name, field_name) VALUES (:table,:column)",array(
            ':table' => $table,
            ':column' => $field_name
    ));
    endif;
    
    return $field;
  }

  /**
   * This maps a generic data type in combination with its data size
   * to the engine-specific data type.
   */
  function getFieldTypeMap() {
  	
    // Put :normal last so it gets preserved by array_flip. This makes
    // it much easier for modules (such as schema.module) to map
    // database types back into schema types.
    // $map does not use drupal_static as its value never changes.
    static $map = array(
      'varchar:normal' => 'varchar',
      'char:normal' => 'char',

      'text:tiny' => 'varchar(2)',
      'text:small' => 'varchar(500)',
      'text:medium' => 'varchar(1000)', 
	  // DB2 deprecated LONG VARCHAR in 9.7 and in some instances, Drupal does comparisons on big:text fields.
      // If we just store blobs this cause issues. A custom 'type' is created with (blob,varchar(32)) to represent
      // the big:text data and an md5sum - we can run comparisions on the checksum. All instances of the custom 
      // type are stored in bigtext_column and loaded into table_information when queryTableInfo() is called.
	  'text:big' => 'bigtext_t',
      'text:normal' => 'varchar(1000)',

      'int:tiny' => 'smallint',
      'int:small' => 'integer',
      'int:medium' => 'integer',
      'int:big' => 'bigint',
      'int:normal' => 'integer',

      'float:tiny' => 'real',
      'float:small' => 'real',
      'float:medium' => 'real',
      'float:big' => 'double',
      'float:normal' => 'real',

      'numeric:normal' => 'integer',

      'blob:big' => 'blob',
      'blob:normal' => 'blob',

      'date:normal' => 'date',

      'datetime:normal' => 'timestamp with local time zone',

      'time:normal'     => 'timestamp',
      // Serial types in DB2 are integer/bigint with IDENTITY tag
      'serial:tiny' => 'integer',
      'serial:small' => 'integer',
      'serial:medium' => 'integer',
      'serial:big' => 'bigint',
      'serial:normal' => 'integer',
      );
    return $map;
  }

  protected function _createKeySql($fields) {
    
    syslog(LOG_ERR,"FUNCTION: _createKeySql");
    $return = array();
    foreach ($fields as $field) {
      if (is_array($field)) {
       	/* DB2 for LUW does support INDEXES on Expresses, removing substr here and passing back
         * the field[] value */       
        //$return[] = 'substr(' . $field[0] . ', 1, ' . $field[1] . ')';
        $return[] = $field[0];
      }
      else {
        $return[] = '"' . $field . '"';
      }
    }
    return implode(', ', $return);
  }

  function renameTable($table, $new_name) {
    
    if (!$this->tableExists($table)) {
      throw new DatabaseSchemaObjectDoesNotExistException(t("Cannot rename %table to %table_new: table %table doesn't exist.", array('%table' => $table, '%table_new' => $new_name)));
    }
    if ($this->tableExists($new_name)) {
      throw new DatabaseSchemaObjectExistsException(t("Cannot rename %table to %table_new: table %table_new already exists.", array('%table' => $table, '%table_new' => $new_name)));
    }

    // Get the schema and tablename for the old table.
    $old_full_name = $this->connection->prefixTables('{' . $table . '}');
    list($old_schema, $old_table_name) = strpos($old_full_name, '.') ? explode('.', $old_full_name) : array('public', $old_full_name);

    // Index names and constraint names are global in PostgreSQL, so we need to
    // rename them when renaming the table.
    $indexes = $this->connection->query('SELECT INDNAME indexname FROM SYSCAT.INDEXES WHERE TABSCHEMA = :schema AND TABNAME = :table', array(':schema' => $old_schema, ':table' => $old_table_name));
    foreach ($indexes as $index) {
      if (preg_match('/^' . preg_quote($old_full_name) . '_(.*)_idx$/', $index->indexname, $matches)) {
        $index_name = $matches[1];
        $this->connection->query('ALTER INDEX ' . $index->indexname . ' RENAME TO {' . $new_name . '}_' . $index_name . '_idx');
      }
    }

    // Now rename the table.
    // Ensure the new table name does not include schema syntax.
    $prefixInfo = $this->getPrefixInfo($new_name);
    $this->connection->query('ALTER TABLE {' . $table . '} RENAME TO ' . $prefixInfo['table']);
  }

  public function dropTable($table) {
    syslog(LOG_ERR,"SCHEMA::FUNC: dropTable");
    if (!$this->tableExists($table)) {
      return FALSE;
    }

    $this->connection->query('DROP TABLE {' . $table . '}');
    return TRUE;
  }

  public function addField($table, $field, $spec, $new_keys = array()) {
    syslog(LOG_ERR,"SCHEMA::FUNC: addField");
    if (!$this->tableExists($table)) {
      throw new DatabaseSchemaObjectDoesNotExistException(t("Cannot add field %table.%field: table doesn't exist.", array('%field' => $field, '%table' => $table)));
    }
    if ($this->fieldExists($table, $field)) {
      throw new DatabaseSchemaObjectExistsException(t("Cannot add field %table.%field: field already exists.", array('%field' => $field, '%table' => $table)));
    }

    $fixnull = FALSE;
    if (!empty($spec['not null']) && !isset($spec['default'])) {
      $fixnull = TRUE;
      $spec['not null'] = FALSE;
    }
    $query = 'ALTER TABLE {' . $table . '} ADD COLUMN ';
    $query .= $this->createFieldSql($field, $this->processField($spec));
    $this->connection->query($query);
    if (isset($spec['initial'])) {
      $this->connection->update($table)
        ->fields(array($field => $spec['initial']))
        ->execute();
    }
    if ($fixnull) {
      $this->connection->query("ALTER TABLE {" . $table . "} ALTER $field SET NOT NULL");
    }
    if (isset($new_keys)) {
      $this->_createKeys($table, $new_keys);
    }
    // Add column comment.
    if (!empty($spec['description'])) {
      $this->connection->query('COMMENT ON COLUMN {' . $table . '}.' . $field . ' IS ' . $this->prepareComment($spec['description']));
    }
  }

  public function dropField($table, $field) {
    syslog(LOG_ERR,"SCHEMA::FUNC: dropField");
    if (!$this->fieldExists($table, $field)) {
      return FALSE;
    }

    $this->connection->query('ALTER TABLE {' . $table . '} DROP COLUMN "' . $field . '"');
    return TRUE;
  }

  public function fieldSetDefault($table, $field, $default) {
    syslog(LOG_ERR,"SCHEMA::FUNC: fieldSetDefault");
    if (!$this->fieldExists($table, $field)) {
      throw new DatabaseSchemaObjectDoesNotExistException(t("Cannot set default value of field %table.%field: field doesn't exist.", array('%table' => $table, '%field' => $field)));
    }

    if (!isset($default)) {
      $default = 'NULL';
    }
    else {
      $default = is_string($default) ? "'$default'" : $default;
    }

    $this->connection->query('ALTER TABLE {' . $table . '} ALTER COLUMN "' . $field . '" SET DEFAULT ' . $default);
  }

  public function fieldSetNoDefault($table, $field) {
    syslog(LOG_ERR,"SCHEMA::FUNC: fieldSerNoDefault");
    if (!$this->fieldExists($table, $field)) {
      throw new DatabaseSchemaObjectDoesNotExistException(t("Cannot remove default value of field %table.%field: field doesn't exist.", array('%table' => $table, '%field' => $field)));
    }

    $this->connection->query('ALTER TABLE {' . $table . '} ALTER COLUMN "' . $field . '" DROP DEFAULT');
  }

  public function indexExists($table, $name) {
    syslog(LOG_ERR,"SCHEMA::FUNC: indexExists");
    // Details http://www.postgresql.org/docs/8.3/interactive/view-pg-indexes.html
    $index_name = '{' . $table . '}_' . $name . '_idx';
    return (bool) $this->connection->query("SELECT 1 FROM SYSCAT.INDEXES WHERE INDNAME = '$index_name'")->fetchField();
  }

  /**
   * Helper function: check if a constraint (PK, FK, UK) exists.
   *
   * @param $table
   *   The name of the table.
   * @param $name
   *   The name of the constraint (typically 'pkey' or '[constraint]_key').
   */
  protected function constraintExists($table, $name) {
    syslog(LOG_ERR,"SCHEMA::FUNC: constraintExists");
    $constraint_name = '{' . $table . '}_' . $name;
    return (bool) $this->connection->query("SELECT 1 FROM pg_constraint WHERE conname = '$constraint_name'")->fetchField();
  }

  public function addPrimaryKey($table, $fields) {
    syslog(LOG_ERR,"FUNCTION: addPrimayKey");
    if (!$this->tableExists($table)) {
      throw new DatabaseSchemaObjectDoesNotExistException(t("Cannot add primary key to table %table: table doesn't exist.", array('%table' => $table)));
    }
    if ($this->constraintExists($table, 'pkey')) {
      throw new DatabaseSchemaObjectExistsException(t("Cannot add primary key to table %table: primary key already exists.", array('%table' => $table)));
    }

    $this->connection->query('ALTER TABLE {' . $table . '} ADD PRIMARY KEY (' . implode(',', $fields) . ')');
  }

  public function dropPrimaryKey($table) {
    syslog(LOG_ERR,"SCHEMA::FUNC: dropPrimayKey");
    if (!$this->constraintExists($table, 'pkey')) {
      return FALSE;
    }

    $this->connection->query('ALTER TABLE {' . $table . '} DROP CONSTRAINT ' . $this->prefixNonTable($table, 'pkey'));
    return TRUE;
  }

  function addUniqueKey($table, $name, $fields) {
    syslog(LOG_ERR,"FUNCTION: addUniqueKey");
    if (!$this->tableExists($table)) {
      throw new DatabaseSchemaObjectDoesNotExistException(t("Cannot add unique key %name to table %table: table doesn't exist.", array('%table' => $table, '%name' => $name)));
    }
    if ($this->constraintExists($table, $name . '_key')) {
      throw new DatabaseSchemaObjectExistsException(t("Cannot add unique key %name to table %table: unique key already exists.", array('%table' => $table, '%name' => $name)));
    }

    $this->connection->query('ALTER TABLE {' . $table . '} ADD CONSTRAINT "' . $this->prefixNonTable($table, $name, 'key') . '" UNIQUE (' . implode(',', $fields) . ')');
  }

  public function dropUniqueKey($table, $name) {
    syslog(LOG_ERR,"SCHEMA::FUNC: dropUniqueKey");
    if (!$this->constraintExists($table, $name . '_key')) {
      return FALSE;
    }

    $this->connection->query('ALTER TABLE {' . $table . '} DROP CONSTRAINT "' . $this->prefixNonTable($table, $name, 'key') . '"');
    return TRUE;
  }

  public function addIndex($table, $name, $fields) {
     syslog(LOG_ERR,"SCHEMA::FUNC: addIndex");
    if (!$this->tableExists($table)) {
      throw new DatabaseSchemaObjectDoesNotExistException(t("Cannot add index %name to table %table: table doesn't exist.", array('%table' => $table, '%name' => $name)));
    }
    if ($this->indexExists($table, $name)) {
      throw new DatabaseSchemaObjectExistsException(t("Cannot add index %name to table %table: index already exists.", array('%table' => $table, '%name' => $name)));
    }

    $this->connection->query($this->_createIndexSql($table, $name, $fields));
  }

  public function dropIndex($table, $name) {
    syslog(LOG_ERR,"SCHEMA::FUNC: dropIndex");   
    if (!$this->indexExists($table, $name)) {
      return FALSE;
    }

    $this->connection->query('DROP INDEX ' . $this->prefixNonTable($table, $name, 'idx'));
    return TRUE;
  }

  public function changeField($table, $field, $field_new, $spec, $new_keys = array()) {
   
    if (!$this->fieldExists($table, $field)) {
      throw new DatabaseSchemaObjectDoesNotExistException(t("Cannot change the definition of field %table.%name: field doesn't exist.", array('%table' => $table, '%name' => $field)));
    }
    if (($field != $field_new) && $this->fieldExists($table, $field_new)) {
      throw new DatabaseSchemaObjectExistsException(t("Cannot rename field %table.%name to %name_new: target field already exists.", array('%table' => $table, '%name' => $field, '%name_new' => $field_new)));
    }

    $spec = $this->processField($spec);

    // We need to typecast the new column to best be able to transfer the data
    // Schema_ibm::getFieldTypeMap() will return possibilities that are not
    // 'cast-able' such as 'serial' - so they need to be casted int instead.
    if (in_array($spec['ibm_type'], array('serial', 'bigserial', 'numeric'))) {
      $typecast = 'int';
    }
    else {
      $typecast = $spec['ibm_type'];
    }

    if (in_array($spec['ibm_type'], array('varchar', 'character', 'text')) && isset($spec['length'])) {
      $typecast .= '(' . $spec['length'] . ')';
    }
    elseif (isset($spec['precision']) && isset($spec['scale'])) {
      $typecast .= '(' . $spec['precision'] . ', ' . $spec['scale'] . ')';
    }

    // Remove old check constraints.
    $field_info = $this->queryFieldInformation($table, $field);

    foreach ($field_info as $check) {
      $this->connection->query('ALTER TABLE {' . $table . '} DROP CONSTRAINT "' . $check . '"');
    }

    // Remove old default.
    $this->fieldSetNoDefault($table, $field);

    $this->connection->query('ALTER TABLE {' . $table . '} ALTER "' . $field . '" TYPE ' . $typecast . ' USING "' . $field . '"::' . $typecast);

    if (isset($spec['not null'])) {
      if ($spec['not null']) {
        $nullaction = 'SET NOT NULL';
      }
      else {
        $nullaction = 'DROP NOT NULL';
      }
      $this->connection->query('ALTER TABLE {' . $table . '} ALTER "' . $field . '" ' . $nullaction);
    }

    if (in_array($spec['ibm_type'], array('serial', 'bigserial'))) {
      // Type "serial" is known to PostgreSQL, but *only* during table creation,
      // not when altering. Because of that, the sequence needs to be created
      // and initialized by hand.
      $seq = "{" . $table . "}_" . $field_new . "_seq";
      $this->connection->query("CREATE SEQUENCE " . $seq);
      // Set sequence to maximal field value to not conflict with existing
      // entries.
      $this->connection->query("SELECT setval('" . $seq . "', MAX(\"" . $field . '")) FROM {' . $table . "}");
      $this->connection->query('ALTER TABLE {' . $table . '} ALTER "' . $field . '" SET DEFAULT nextval(\'' . $seq . '\')');
    }

    // Rename the column if necessary.
    if ($field != $field_new) {
      $this->connection->query('ALTER TABLE {' . $table . '} RENAME "' . $field . '" TO "' . $field_new . '"');
    }

    // Add unsigned check if necessary.
    if (!empty($spec['unsigned'])) {
      $this->connection->query('ALTER TABLE {' . $table . '} ADD CHECK ("' . $field_new . '" >= 0)');
    }

    // Add default if necessary.
    if (isset($spec['default'])) {
      $this->fieldSetDefault($table, $field_new, $spec['default']);
    }

    // Change description if necessary.
    if (!empty($spec['description'])) {
      $this->connection->query('COMMENT ON COLUMN {' . $table . '}."' . $field_new . '" IS ' . $this->prepareComment($spec['description']),$table,$field_new);
    }

    if (isset($new_keys)) {
      $this->_createKeys($table, $new_keys);
    }
  }
  
  /**
   * Return a list of columns for an index definition.
   */
  protected function _createIndexSql($table, $name, $fields) {
    $query = 'CREATE INDEX "' . $this->prefixNonTable($table, $name, 'idx') . '" ON {' . $table . '} (';
    $query .= $this->_createKeySql($fields) . ')';
    return $query;
  }

  protected function _createKeys($table, $new_keys) {
    syslog(LOG_ERR,"FUNCTION: _createKeys");
    if (isset($new_keys['primary key'])) {
      $this->addPrimaryKey($table, $new_keys['primary key']);
    }
    if (isset($new_keys['unique keys'])) {
      foreach ($new_keys['unique keys'] as $name => $fields) {
        $this->addUniqueKey($table, $name, $fields);
      }
    }
    if (isset($new_keys['indexes'])) {
      foreach ($new_keys['indexes'] as $name => $fields) {
        $this->addIndex($table, $name, $fields);
      }
    }
  }

  /**
   * Retrieve a table or column comment.
   */
  public function getComment($table, $column = NULL) {
    syslog(LOG_ERR,"FUNCTION: getComment");
    $info = $this->getPrefixInfo($table);
    // Don't use {} around pg_class, pg_attribute tables.
    if (isset($column)) {
      return $this->connection->query('SELECT col_description(oid, attnum) FROM pg_class, pg_attribute WHERE attrelid = oid AND relname = ? AND attname = ?', array($info['table'], $column))->fetchField();
    }
    else {
      return $this->connection->query('SELECT obj_description(oid, ?) FROM pg_class WHERE relname = ?', array('pg_class', $info['table']))->fetchField();
    }
  }
  
  public static function tableSchema($table) {
    syslog(LOG_ERR,"SCHEMA::FUNC: tableSchema");
    $exp= explode(".",$table);
     
    if (count($exp)>1)
    return strtoupper(str_replace('"','',$exp[0]));
    else
    return false;
  }
  
  // @todo - fix this!!!!!!!!!!!
  public function tableExists($table) {
    
    $schema= $this->tableSchema($this->connection->prefixTables('{' . strtoupper($table) . '}',true));
    //syslog(LOG_ERR,"SCHEMA::FUNC: tableExists => TABLE MAYBE EXIST and TABLE is: " .$table);
    /* Table is going to be created in schema, if no schema we can safely return false */
    if ($schema) {
      $retval= db_query("SELECT 1 as Expression FROM  syscat.tables WHERE tabname = ?",array(strtoupper($table)))->fetchField();
      if ($retval){
        syslog(LOG_ERR,"SCHEMA::FUNC: tableExists => TABLE DOES EXIST");
        return true;
      }
    } else {
      return false;
    }
  }
  
  protected function createColsSql($cols) {
    $ret = array();
    foreach ($cols as $col) $ret[] = $col;
    return implode(', ', $ret);
  }
  
  /**
   * Override of DatabaseSchema::prepareComment()
   * DB2 is has a lenght limit on comments, need to concat for now
   * @todo - figure out way to reassemble comments.
   */
  public function prepareComment($comment, $name, $field, $length = NULL) {
    if(strlen($comment) > IBM_MAX_COMMENT_LENGTH) {
    	syslog(LOG_ERR, "prepareComment name: " .print_r($field,true));
/*     $this->connection->query("INSERT INTO col_comments VALUES (:table,:field,:leftover)", array(
    ':table' => $name,
    ':field' => $field,
    ':leftover' => substr($comment, IBM_MAX_COMMENT_LENGTH)
    )); */
    return substr($this->connection->quote($comment),0,IBM_MAX_COMMENT_LENGTH) . "'";
    } else {
      return $this->connection->quote($comment);
    }
  }
    
  // add here
}
