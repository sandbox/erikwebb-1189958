<?php

/**
 * @ingroup database
 * @{
 */

/**
 * @file
 * Query code for DB2 database engine.
 */
 
class InsertQuery_ibm extends InsertQuery {
	
	public function execute() {
		// If validation fails, simply return NULL. Note that validation routines
		// in preExecute() may throw exceptions instead.
		if (!$this->preExecute()) {
			return NULL;
		}
		
		// Fetch the list of blobs, bigtext_t and sequences used on that table.
		$table_information = $this->connection->schema()->queryTableInformation($this->table);
		$query = (string)$this;
		
		// If any insert fields are of type BIGTEXT_T, need to modify the query before it is prepared and bound.
		// UPDATE - PDO doesn't play nice with UDTs in DB2. Looking at the PDO_IBM code it doesn't appear that 
		// the driver was ever meant to support them. I filed a bug: https://bugs.php.net/bug.php?id=60277
		// Leaving the following code for now. Moving to creating a DISTINCT TYPE for text:big fields and creating
		// a method to return hashed value
		if(isset($table_information->bigtext_fields)) {
			syslog(LOG_ERR,"bigtext_fields idx => " . print_r($table_information->bigtext_fields, true));
				foreach($table_information->bigtext_fields as $bigtext_field => $val) {
					//syslog(LOG_ERR,"bigtext_field => " . $bigtext_field . " val is: ". $val ." this->fields = " . print_r($this->insertFields,true));
					//$bigtext_idx = array_search($bigtext_field, $this->insertFields);
					$placeholder = ':db_insert_placeholder_' . array_search($bigtext_field, $this->insertFields);
					$query = preg_replace("/$placeholder/","bigtext_t($placeholder, :md5sum)",$query);
					syslog(LOG_ERR,"MODIFIES BIGTEXT QUERY " . $query);
				}
		}
		
		$stmt = $this->connection->prepareQuery($query)->getStatement(NULL); 
		
		// Bind params properly for blob fields
		$max_placeholder = 0;
		$blobs = array();
		$blob_count = 0;
		foreach ($this->insertValues as $insert_values) {
			foreach ($this->insertFields as $idx => $field) {
				if (isset($table_information->blob_fields[$field])) {
					// The serializing of data here as MSSQL & PGSQL do caused some very low level issue with the install
					// causing a 'Page Not Found' err on completion. No clear reason why. Seems some cache wasn't updated.
					// I'd like to be able to use is. Therefore, leaving blob check here in place. It doesn't really
					// do anything at the moment (even with PARAM_LOB).
					$stmt->bindParam(':db_insert_placeholder_' . $max_placeholder++, $insert_values[$idx], PDO::PARAM_LOB);
					// Pre-increment is faster in PHP than increment.
					++$blob_count;
				}
				elseif (isset($table_information->bigtext_fields[$field])) {
					$stmt->bindParam(':db_insert_placeholder_' . $max_placeholder++, $insert_values[$idx]);
					// Drupal does DISTINCT on some big text fields. We can't do that on blobs and DB2
					// has deprecated LONG VARCHAR. We'll store an MD5 somewhere (not in a UDT for now!) and
					// the make distincts on text:big use the hashes
					$md5 = md5($insert_values[$idx]);
					$stmt->bindParam(':md5sum',$md5);
				}
				else {
					$stmt->bindParam(':db_insert_placeholder_' . $max_placeholder++, $insert_values[$idx]);
				}
			}
		}
		
		// Deal w/ fromQuery
		if (!empty($this->fromQuery)) {
			//syslog(LOG_ERR,"Query.inc Line 58");
			// bindParam stores only a reference to the variable that is followed when
			// the statement is executed. We pass $arguments[$key] instead of $value
			// because the second argument to bindParam is passed by reference and
			// the foreach statement assigns the element to the existing reference.
			$arguments = $this->fromQuery->getArguments();
			foreach ($arguments as $key => $value) {
				$stmt->bindParam($key, $arguments[$key]);
			}
		}
		
		// Initialize options array to pass back to query. If there is a sequence, we can get the 
		// insert_id from lastInsertId function called in the 'switch' statement
		$options = $this->queryOptions;
		
		if (isset($table_information->sequence)) {
			//syslog(LOG_ERR,"Query.inc Line 78 sequence_name => " .  $table_information->sequence);
			$options['sequence_name'] = $table_information->sequence;
			$this->queryOptions['return'] = Database::RETURN_INSERT_ID;
		}
		// If there are no sequences then we can't get a last insert id.
		elseif ($options['return'] == Database::RETURN_INSERT_ID) {

			$options['return'] = Database::RETURN_NULL;
		}
		
		// Only use the returned last_insert_id if it is not already set.
		if (!empty($last_insert_id)) {
			
			$this->connection->query($stmt, array(), $options);
			//syslog(LOG_ERR,"Query.inc Line 92 last_insert_id => NOT SET options: " . print_r($options,true));
		}
		else {
			$last_insert_id = $this->connection->query($stmt, array(),$options);
			//syslog(LOG_ERR,"Query.inc Line 96 last_insert_id => " . $last_insert_id . " options: " . print_r($options,true));
		}
		
		if(isset($last_insert_id))
		syslog(LOG_ERR,"LastInsertId is " . $last_insert_id . " FOR QUERY " . $query);
		
		// Re-initialize the values array so that we can re-use this query.
		$this->insertValues = array();
		
		return $last_insert_id;
	}
	
	public function __toString() {
		// Create a sanitized comment string to prepend to the query.
		$comments = $this->connection->makeComment($this->comments);
		
		// Default fields are always placed first for consistency.
		$insert_fields = array_merge($this->defaultFields, $this->insertFields);
		
		// If we're selecting from a SelectQuery, finish building the query and
		// pass it back, as any remaining options are irrelevant.
		if (!empty($this->fromQuery)) {
			return $comments . 'INSERT INTO {' . $this->table . '} (' . implode(', ', $insert_fields) . ') ' . $this->fromQuery;
		}
		
		$query = $comments . 'INSERT INTO {' . $this->table . '} (' . implode(', ', $insert_fields) . ') VALUES ';
		
		$max_placeholder = 0;
		$values = array();
		if (count($this->insertValues)) {
			foreach ($this->insertValues as $insert_values) {
				$placeholders = array();
		
				// Default fields aren't really placeholders, but this is the most convenient
				// way to handle them.
				$placeholders = array_pad($placeholders, count($this->defaultFields), 'default');
		
				$new_placeholder = $max_placeholder + count($insert_values);
				for ($i = $max_placeholder; $i < $new_placeholder; ++$i) {
					
					$placeholders[] = ':db_insert_placeholder_' . $i;
					
				}
				$max_placeholder = $new_placeholder;
				$values[] = '(' . implode(', ', $placeholders) . ')';
			}
		}
		else {
			// If there are no values, then this is a default-only query. We still need to handle that.
			$placeholders = array_fill(0, count($this->defaultFields), 'default');
			$values[] = '(' . implode(', ', $placeholders) . ')';
		}
		
		$query .= implode(', ', $values);

		return $query;
	}
}

class UpdateQuery_ibm extends UpdateQuery {
	
	public function execute() {
		
		// need to handle BLOBS properly
		$max_placeholder = 0;
		$blobs = array();
		$blob_count = 0;
		
		// Because we filter $fields the same way here and in __toString(), the
		// placeholders will all match up properly.
		$stmt = $this->connection->prepareQuery((string)$this)->getStatement(NULL);
		
		// Fetch the list of blobs and sequences used on that table.
		$table_information = $this->connection->schema()->queryTableInformation($this->table);
	
		// Expressions take priority over literal fields, so we process those first
		// and remove any literal fields that conflict.
		$fields = $this->fields;
		$expression_fields = array();
		foreach ($this->expressionFields as $field => $data) {
			if (!empty($data['arguments'])) {
				foreach ($data['arguments'] as $placeholder => $argument) {
					// We assume that an expression will never happen on a BLOB field,
					// which is a fairly safe assumption to make since in most cases
					// it would be an invalid query anyway.
					$stmt->bindParam($placeholder, $data['arguments'][$placeholder]);
					}
				}
			unset($fields[$field]);
		}
	
		// Because we filter $fields the same way here and in __toString(), the
		// placeholders will all match up properly.
		foreach ($fields as $field => $value) {
			$placeholder = ':db_update_placeholder_' . ($max_placeholder++);
			// set bindParam if $value is blob
			if (isset($table_information->blob_fields[$field])) {
				// there is an issue here is you serialize the data - checking for the blob field doesn't 
				// effect the insert and the PARAM_LOB doesn't do anything here. Only leaving this code here
				// in case I decide to fix later. - Same for UpdateQuery
				$stmt->bindParam($placeholder, $fields[$field], PDO::PARAM_LOB);
				++$blob_count;
			}
			elseif (isset($table_information->bigtext_fields[$field])) {

			}
			else {
				//syslog(LOG_ERR,"Line 132 UpdateQuery, non-blob field => " . $field . " = " . $fields[$field]);
				$stmt->bindParam($placeholder, $fields[$field]);
			}
		}
	
		if (count($this->condition)) {
			$this->condition->compile($this->connection, $this);
			$arguments = $this->condition->arguments();
			foreach ($arguments as $placeholder => $value) {
			//syslog(LOG_ERR,"Line 146 UpdateQuery, this->condition value => " . $value);
			$stmt->bindParam($placeholder, $arguments[$placeholder]);
			}
		}
		
		$options = $this->queryOptions;
		$options['already_prepared'] = TRUE;

		$this->connection->query($stmt, array(), $options);
	
		return $stmt->rowCount();
	}
	
	public function __toString() {
		// Create a sanitized comment string to prepend to the query.
		$comments = $this->connection->makeComment($this->comments);
	
		// Expressions take priority over literal fields, so we process those first
		// and remove any literal fields that conflict.
		$fields = $this->fields;
		$update_fields = array();
		foreach ($this->expressionFields as $field => $data) {
			$update_fields[] = $field . '=' . $data['expression'];
			unset($fields[$field]);
		}
	
		$max_placeholder = 0;
		foreach ($fields as $field => $value) {
			$update_fields[] = $field . '=:db_update_placeholder_' . ($max_placeholder++);
		}
	
		$query = $comments . 'UPDATE {' . $this->connection->escapeTable($this->table) . '} SET ' . implode(', ', $update_fields);
	
		if (count($this->condition)) {
			$this->condition->compile($this->connection, $this);
			// There is an implicit string cast on $this->condition.
			$query .= "\nWHERE " . $this->condition;
		}
		syslog(LOG_ERR,"LINE 247 UpdateQuery_toString: " . $query);
		return $query;
	}
}

class TruncateQuery_ibm extends TruncateQuery {

	// Using the TRUNCATE command in DB2 is a pain as it must be the the first action in a
	// 'unit of work' - this requries doing funny things with cursors and a lot of extra
	// code for something that is only done during install. The following accomplishes the
	// same thing functionally as the TRUNCATE command
	public function __toString() {

		return 'ALTER TABLE '. $this->connection->escapeTable($this->table) . ' ACTIVATE NOT LOGGED INITIALLY WITH EMPTY TABLE';
	}
}