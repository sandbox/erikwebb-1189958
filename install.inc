<?php

// DB2 specific install functions
// include_once DRUPAL_ROOT . '/includes/database/ibm/schema.inc';

class DatabaseTasks_ibm extends DatabaseTasks {

  protected $pdoDriver = 'ibm';

  public function name() {
    return 'IBM';
  }

  public function __construct() {
    // Core tasks are using a table without primary key, they need to be
    // completely rewritten.
    $this->tasks = array();

    // Create the user-defined functions we need to be Drupal friendly.
    $this->tasks[] = array(
      'function' => 'initializeDatabase',
      'arguments' => array(),
    );
  }
  
/*  public function name() {
    return st('DB2');
  }*/

  public function minimumVersion() {
    return '9.7';
  }

  private function ibm_query($sql, $args=NULL) {
    return Database::getConnection('default')->ibmQuery($sql,$args);
  }

  /**
   * Make DB2 Drupal friendly.
   */
  function initializeDatabase() {
    
    // We only want to create additional DB object 1 time, DrupaL runs through a couple times
    // during the install process. We'll search for the BLOB_COLUMN table. If not found(0) we create
    // all objects, if found(1) we skip.
    $pass = $this->ibm_query("select 1 as expression from syscat.tables where tabname = 'BLOB_COLUMN'");
    
    switch ($pass->fetchColumn()) {
      case 0:
            try {
            syslog(LOG_ERR,"current dir: ".getcwd());
            $this->create_failsafe_objects("includes/database/ibm/resources/tables");
            $this->create_failsafe_objects("includes/database/ibm/resources/sequence");
            
            // Since DB2 Ex has no support for PL/SQL we have to create modules and then add
            // add/publish functions. Called with MODULE.function(args)
            $this->create_modules("includes/database/ibm/resources/modules");
            $this->create_failsafe_objects("includes/database/ibm/resources/types");
            $this->create_failsafe_objects("includes/database/ibm/resources/functions");
            $this->create_failsafe_objects("includes/database/ibm/resources/transform");
            $this->create_failsafe_objects("includes/database/ibm/resources/triggers");
            $this->pass(st('DB2 has initialized itself.'));
            } catch (Exception $e) {
            syslog(LOG_ERR,$e->getMessage());
            $this->fail(st('Drupal could not be correctly setup with the existing database. Revise any errors.'));
            }
      case 1: 
            $this->pass(st('DB2 has initialized itself.'));
      }
   }

  /**
   * Supporting functions to help create stuff (tables,modules,functions,etc) in DB2
   * May not need all
   * @todo - remove what's not needed!
   * Couple of notes about object types used in Oracle driver, DB2 supports collections
   * using PL/SQL but can't use that with DB2 Express, inline triggers are supported though
   * Look into using.
   */
  private function create_sp_objects($dir_path)
  {
    $dir = opendir($dir_path);
     
    while($name = readdir($dir))
    {
      if (in_array($name,array('.','..','.DS_Store','CVS'))) continue;
      if (is_dir($dir_path."/".$name))
      $this->create_sp_object($dir_path."/".$name);
    }
  }

  private function create_sp_object($dir_path)
  {
    $dir = opendir($dir_path);
     
    $spec= "";
    $body= "";
     
    while ($name = readdir($dir))
    {
      if (substr($name,-4)=='.pls')
      $spec= $name;
      else
      if (substr($name,-4)=='.plb')
      $body= $name;
    }
     
    $this->create_object($dir_path."/".$spec);
     
    if ($body)
    $this->create_object($dir_path."/".$body);
  }

  private function create_objects($dir_path)
  {
    $dir = opendir($dir_path);
     
    while($name = readdir($dir))
    {
      if (in_array($name,array('.','..','.DS_Store','CVS'))) continue;
      $this->create_object($dir_path."/".$name);
    }
  }

  private function create_object($file_path)
  {
    syslog(LOG_ERR,"creating object: $file_path");
     
    try
    {
      $this->ibm_query($this->get_php_contents($file_path));
    }
    catch (Exception $ex)
    {
      syslog(LOG_ERR,"object $file_path created with errors");
    }
  }

  private function create_modules($dir_path)
  {
    $dir = opendir($dir_path);
     
    while($name = readdir($dir))
    {
      if (in_array($name,array('.','..','.DS_Store','CVS'))) continue;
      $this->create_module($dir_path."/".$name);
    }
  }

  private function create_module($file_path)
  {
    syslog(LOG_ERR,"creating object: $file_path");
     
    try
    {
      foreach (file($file_path) as $lines => $line){
        syslog(LOG_ERR,"Line is " . $line);
        $this->ibm_query($line);
      }
    }
    catch (Exception $ex)
    {
      syslog(LOG_ERR,"object $file_path created with errors");
    }
  }

  private function create_failsafe_objects($dir_path)
  {
    $dir = opendir($dir_path);
     
    while($name = readdir($dir))
    {
      if (in_array($name,array('.','..','.DS_Store','CVS'))) continue;

      syslog(LOG_ERR,"creating object: $dir_path/$name");
      $this->failsafe_ddl($this->get_php_contents($dir_path."/".$name));
    }
  }

  private function failsafe_ddl($ddl)
  {
    $this->ibm_query("begin execute immediate '".str_replace("'","''",$ddl)."'; end");
  }

  private function get_php_contents($filename)
  {
    if (is_file($filename))
    {
      ob_start();
      require_once $filename;
      $contents = ob_get_contents();
      ob_end_clean();
      return $contents;
    }
    else
    syslog(LOG_ERR,"error: file ".$filename." does not exists");

    return false;
  }

}
