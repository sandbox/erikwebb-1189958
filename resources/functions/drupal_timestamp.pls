alter module drupal_timestamp add function to_date (N integer) returns varchar(20) begin return varchar_format(CHAR(timestamp('1970-01-01','00.00.00') + int(N/86400) days + int(mod(N,86400)) seconds),'YYYY-MM-DD HH24:MI:SS'); end

