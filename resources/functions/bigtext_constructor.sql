create function bigtext_t(content blob(2M), md5sum varchar(32)) returns bigtext_t return bigtext_t() ..content(content) ..md5sum(md5sum)
