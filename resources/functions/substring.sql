ALTER MODULE SUBSTRING ADD FUNCTION SUBSTRING(buff varchar(8000), start integer, length integer default null) RETURNS VARCHAR(8000) begin if length is null then return SUBSTR(buff, start); else return SUBSTR(buff, start, length); end if; end

