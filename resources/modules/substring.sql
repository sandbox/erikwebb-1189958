create or replace module SUBSTRING
ALTER MODULE SUBSTRING PUBLISH FUNCTION SUBSTRING_INDEX(buff varchar(8000), delimiter char(1), count integer) RETURNS VARCHAR(8000)
ALTER MODULE SUBSTRING PUBLISH FUNCTION SUBSTRING(buff varchar(8000), start integer, length integer default null) RETURNS VARCHAR(8000)
