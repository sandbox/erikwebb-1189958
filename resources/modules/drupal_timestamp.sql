create or replace module drupal_timestamp
alter module drupal_timestamp publish function to_date(N integer) returns varchar(20)
